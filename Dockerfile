FROM node:16.16.0-alpine3.16
WORKDIR /usr/src/app
COPY package*json .
RUN npm ci
COPY . .

ENV ME_CONFIG_MONGODB_URL=mongodb://usermode:remoSenha123456@localhost:27017
# Build the app for production
RUN npm run build

EXPOSE 3001

CMD [ "npm", "start" ]
