import { Request, Response } from 'express';
import livros from '../model/livros';

class LivroController {
  static listarLivros = async (req: Request, res: Response) => {
    const response = await livros.find().populate('autor').exec();

    res.status(200).json(response);
  };

  static listarLivrosPorId = async (req: Request, res: Response) => {
    const id = req.params.id;
    const response = await livros.findById(id).populate('autor', 'nome').exec();
    res.status(200).json(response);
  };

  static cadastrarLivro = async (req: Request, res: Response) => {
    const livro = new livros(req.body);

    await livro.save();

    res.json({ message: 'foi' });
  };
  static atualizarLivro = async (req: Request, res: Response) => {
    const id = req.params.id;

    await livros.findByIdAndUpdate(id, { $set: req.body });

    res.json({ message: true });
  };

  static excluirLivro = async (req: Request, res: Response) => {
    const id = req.params.id;
    await livros.findByIdAndDelete(id);
    res.status(200).json({ message: true });
  };

  static listarLivroPorEditora = async (req: Request, res: Response) => {
    const editora = req.query.editora;

    const response = await livros.find({ editora: editora });

    res.status(200).json(response);
  };
}

export default LivroController;
