import { Request, Response } from 'express';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import users from '../model/users';

class AuthController {
  static login = async (req: Request, res: Response) => {
    try {
      const { email, password } = req.body;
      const user = await users.findOne({
        email_address: email
      });

      if (!user) {
        res.status(404).json({ message: 'user not found' });
        return;
      }

      const verify = await bcrypt.compare(password, user.password);

      if (!verify) {
        res.status(404).json({ message: 'user not found' });
        return;
      }

      const token = jwt.sign({ id: user.id }, '1546541324', {
        expiresIn: '1h'
      });

      const newUser = {
        email_address: user?.email_address,
        name: user?.full_name
      };

      res.status(200).json({ user: newUser, token });
      return;
    } catch (error) {
      res.status(400).json({ message: error });
      return;
    }
  };

  static getProfile = async (req: Request, res: Response) => {
    return res.json(req.user);
  };
}

export default AuthController;
