import { Request, Response } from 'express';
import bcrypt from 'bcrypt';
import users from '../model/users';

class UserController {
  static createUser = async (req: Request, res: Response) => {
    try {
      const user = req.body;

      const hashPassword = await bcrypt.hash(user.password, 10);

      const newUser = new users({ ...user, password: hashPassword });
      await newUser.save();
      res
        .status(200)
        .json({ message: 'user criado com sucesso', response: newUser });
      return;
    } catch (error) {
      console.log('Error ', error);
      res.status(400).json(error);
      return;
    }
  };

  static getUsers = async (req: Request, res: Response) => {
    const list = await users.find();
    console.log(list);
    res.status(200).json({ users: list });
    return;
  };
}

export default UserController;
