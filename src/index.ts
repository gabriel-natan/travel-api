import db from './db';
import app from './app';

db.on('error', console.log.bind(console, 'ERRO DO BANCO'));

db.once('open', () => {
  console.log('Conexão realizada com sucesso');
});

const port = process.env.PORT || 3001;

app.listen(port, () => {
  console.log('app runner');
});
