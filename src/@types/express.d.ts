import { IUser } from '../model/users';

declare global {
  namespace Express {
    export interface Request {
      user: Partial<IUser>;
    }
  }
}
