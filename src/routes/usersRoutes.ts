import express from 'express';
import UsersController from '../controllers/userController';

const router = express.Router();

router
  .get('/users', UsersController.getUsers)
  .post('/users', UsersController.createUser);
// .get('/livros/busca', LivroController.listarLivroPorEditora)
// .get('/livros/:id', LivroController.listarLivrosPorId)
// .put('/livros/:id', LivroController.atualizarLivro)
// .delete('/livros/:id', LivroController.excluirLivro);

export default router;
