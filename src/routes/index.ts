import express, { Express, Request, Response } from 'express';
import livros from './livrosRoutes';
import users from './usersRoutes';
import auth from './authRoutes';
import cors from 'cors';

const routes = (app: Express) => {
  app.route('/').get((req: Request, res: Response) => {
    res.status(200).send({ titulo: 'Curso de node' });
  });

  app.use(express.json(), cors(), livros, users, auth);
};

export default routes;
