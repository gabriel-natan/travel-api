import express from 'express';
import AuthController from '../controllers/authController';
import { authMiddleware } from '../middleware/authMiddleware';

const router = express.Router();
router.use('/auth', authMiddleware);
router.post('/login', AuthController.login);
router.post('/auth/profile', AuthController.getProfile);
// .post('/users', AuthController.createUser);
// .get('/livros/busca', LivroController.listarLivroPorEditora)
// .get('/livros/:id', LivroController.listarLivrosPorId)
// .put('/livros/:id', LivroController.atualizarLivro)
// .delete('/livros/:id', LivroController.excluirLivro);

export default router;
