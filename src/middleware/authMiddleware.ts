import jwt from 'jsonwebtoken';
import { NextFunction, Request, Response } from 'express';
import users, { IUser } from '../model/users';

interface JwsPayload {
  id: string;
}

export const authMiddleware = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const { authorization } = req.headers;
    const token = authorization?.split(' ')[1];
    const { id } = jwt.verify(token ?? '', '1546541324') as JwsPayload;
    const profile = await users.findById(id);
    if (profile) {
      req.user = profile as IUser;
    }
    next();
  } catch (error) {
    res.status(400).json({ message: 'erro' });
  }
};
