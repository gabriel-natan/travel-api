import mongoose from 'mongoose';

mongoose.connect(
  `${
    process.env.ME_CONFIG_MONGODB_URL
      ? process.env.ME_CONFIG_MONGODB_URL
      : 'mongodb://usermode:remoSenha123456@localhost:27017'
  }`
);

const db = mongoose.connection;

export default db;
