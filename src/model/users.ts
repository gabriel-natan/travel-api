import mongoose, { Document, Model } from 'mongoose';

export interface IUser extends Document {
  full_name: string;
  email_address: string;
  password: string;
  date_of_birth: Date;
  phone_number: string;
  country_of_residence: string;
  travel_preferences: string[];
  travel_history: string[];
  payment_information: {
    card_number: string;
    expiration_date: Date;
    cvv: string;
  };
}

const UserSchema = new mongoose.Schema<IUser>({
  full_name: { type: String, required: true },
  email_address: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  date_of_birth: { type: Date, required: true },
  phone_number: { type: String, required: true },
  country_of_residence: { type: String, required: true },
  travel_preferences: { type: [String], required: true },
  travel_history: { type: [String], required: true },
  payment_information: {
    card_number: {
      type: String,
      required: true
    },
    expiration_date: {
      type: Date,
      required: true
    },
    cvv: {
      type: String,
      required: true
    }
  }
});

const users: Model<IUser> = mongoose.model('users', UserSchema);

export default users;
